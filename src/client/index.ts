import Scene from "./scene";
import {spheres} from "./spheres-list";

const scene = new Scene()
scene.addPlane('#757575')
scene.addLight(-5, 3, 2)
scene.addLight(5, 2, 6)

// task1(scene)
task2(scene)

animate()

function animate() {
    requestAnimationFrame(animate)
    scene.render()
}

function task1(scene: Scene) {
    scene.addBox(0, 0.5, 0, 5, 1.7, 3)
    spheres.forEach(sphere => scene.addSphere(sphere))
}


function task2(scene: Scene) {
    const flask1 = scene.addFlask(0, 0, 0, 0)
    flask1.setRotation(45)
    flask1.setPosition(8, 0, 4)

    const flask2 = scene.addFlask(0, 0, 0, 0)
    flask2.setPosition(0, 0, -2)
    flask2.setRotation(150)

    const flask3 = scene.addFlask(0, 0, 0, 0)
    flask3.setPosition(-1, 0, 8)
    flask3.setRotation(-95)
}
