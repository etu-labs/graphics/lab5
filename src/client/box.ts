import {Mesh, MeshStandardMaterial, Scene} from 'three'
import * as THREE from "three";
import ComplexObject from "./complex-object";
import {degreesToRad} from "./utils";

class Box extends ComplexObject {
    private readonly material: MeshStandardMaterial
    private readonly wallWidth: number = 0.1

    constructor(
        x: number,
        y: number,
        z: number,
        private readonly width: number,
        private readonly height: number,
        private readonly depth: number,
    ) {
        super(x, y, z);
        const material = new THREE.MeshStandardMaterial({
            roughness: 0.7,
            color: 0xffffff,
            bumpScale: 0.002,
            metalness: 0.2
        });

        new THREE.TextureLoader().load( 'img/wood4.jpeg', function ( map ) {
            map.wrapS = THREE.RepeatWrapping;
            map.wrapT = THREE.RepeatWrapping;
            map.anisotropy = 4;
            map.repeat.set( 1, 1 );
            map.encoding = THREE.sRGBEncoding;
            material.map = map;
            material.needsUpdate = true;
        });

        this.material = material

        this.addWall(x, y, z, width, height, 0)
        this.addWall(x - width / 2 + this.wallWidth / 2, y, z + depth / 2, depth, height, 90)
        this.addWall(x, y, z + depth, width, height, 0)
        this.addWall(x + width / 2 - this.wallWidth / 2, y, z + depth / 2, depth, height, 90)
        this.addBottom()
        this.addLid(45)
    }

    private addWall(x: number, y: number, z: number, width: number, height: number, rotate: number) {
        const boxGeometry = new THREE.BoxGeometry(width, height, this.wallWidth);
        const boxMesh = new THREE.Mesh(boxGeometry, this.material);
        boxMesh.position.set(x, y, z);
        boxMesh.rotation.set(0, degreesToRad(rotate), 0)
        boxMesh.castShadow = true;
        boxMesh.receiveShadow = true;
        this.addObject(boxMesh)
    }

    private addBottom() {
        const boxGeometry = new THREE.BoxGeometry( this.width, this.wallWidth, this.depth);
        const boxMesh = new THREE.Mesh( boxGeometry, this.material );
        boxMesh.position.set(this.x, 0, this.z + this.depth / 2);
        boxMesh.castShadow = true;
        boxMesh.receiveShadow = true;
        this.addObject(boxMesh)
    }

    private addLid(rotation: number) {
        const rad = degreesToRad(rotation)
        const boxGeometry = new THREE.BoxGeometry( this.width, this.depth, this.wallWidth);
        const boxMesh = new THREE.Mesh( boxGeometry, this.material );
        boxMesh.position.set( 0, this.height + (this.depth * Math.sin(rad))/2 - this.y + this.wallWidth, this.z + (this.depth/2 * Math.cos(rad)) - this.wallWidth / 4);
        boxMesh.rotation.set(rad, 0, 0)
        boxMesh.castShadow = true;
        boxMesh.receiveShadow = true;
        this.addObject(boxMesh)
    }
}

export default Box