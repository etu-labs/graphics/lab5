import * as THREE from 'three'
import {SceneLight} from "./types/scene-light";
import {Sphere} from "./types/sphere";
import {OrbitControls} from "three/examples/jsm/controls/OrbitControls";
import Box from "./box";
import Flask from "./flask";
import ComplexObject from "./complex-object";

class Scene {
    private readonly scene: THREE.Scene
    private lights: SceneLight[] = []
    private readonly hemiLight: THREE.HemisphereLight
    private readonly camera: THREE.PerspectiveCamera
    private readonly renderer: THREE.WebGLRenderer
    private readonly textureLoader = new THREE.TextureLoader()

    constructor() {
        this.scene = new THREE.Scene()
        // this.scene.fog = new THREE.Fog( 0x000000, 250, 1400 );
        this.scene.background = new THREE.Color('#fff')
        this.camera = this.createCamera()
        this.renderer = this.createRenderer()
        this.hemiLight = this.addHemiLight()
        this.initControls()
        window.addEventListener( 'resize', this.onWindowResize);
    }

    public addLight(x: number, y: number, z: number) {
        const geometry = new THREE.SphereGeometry(0.1, 32, 32);
        const light = new THREE.PointLight(0xffffff, 1, 100, 2);

        const material = new THREE.MeshStandardMaterial( {
            emissive: 0xffffff,
            emissiveIntensity: 1,
            color: 0x000000
        });

        light.add(new THREE.Mesh(geometry, material));
        light.position.set(x, y, z);
        light.castShadow = true;
        this.lights.push({light, material})
        this.scene.add(light);
    }

    public addSphere(data: Sphere) {
        const material = new THREE.MeshStandardMaterial( {
            color: data.color || '#1f2e22',
            roughness: 0.5,
            metalness: 1
        } );

        if (data.texture) {
            this.textureLoader.load( 'textures/' + data.texture, function ( map ) {
                map.anisotropy = 4;
                map.encoding = THREE.sRGBEncoding;
                material.color = new THREE.Color('#fff')
                material.map = map;
                material.needsUpdate = true;
            } );
        }

        const geometry = new THREE.SphereGeometry( data.radius, 32, 32 );
        const mesh = new THREE.Mesh(geometry, material);
        mesh.position.set(data.x, data.radius, data.z);
        mesh.rotation.y = Math.PI;
        mesh.castShadow = true;
        mesh.receiveShadow = true
        this.scene.add(mesh);
    }

    public addBox(x: number, y: number, z: number, width: number, height: number, depth: number) {
        const box = new Box(x, y, z, width, height, depth)
        box.addToScene(this.scene)
    }

    public addFlask(x: number, y: number, z: number, rotation: number): ComplexObject {
        const flask = new Flask(x, y, z)
        flask.addToScene(this.scene)
        return flask
    }

    public addPlane(color?: string) {
        const geometry = new THREE.PlaneGeometry( 40, 40 );
        const material = new THREE.MeshStandardMaterial( {
            // roughness: 0.8,
            color: color || '#816656',
            // metalness: 0.2,
            // bumpScale: 0.0005,
            opacity: 0.9,
            transparent: true
        });
        const mesh = new THREE.Mesh(geometry, material);
        mesh.receiveShadow = true;
        mesh.castShadow = true;
        mesh.rotation.x = - Math.PI / 2.0;
        this.scene.add(mesh);
    }

    public render() {
        this.renderer.toneMappingExposure = Math.pow(0.68, 5.0 );
        this.renderer.shadowMap.enabled = true;

        this.lights.forEach((l) => {
            l.light.castShadow = true;
            l.light.power = 5000;
            l.material.emissiveIntensity = l.light.intensity / Math.pow( 0.02, 2.0 );
        })

        this.hemiLight.intensity = 5;
        this.renderer.render(this.scene, this.camera);
    }

    private addHemiLight(): THREE.HemisphereLight {
        const hemisphereLight = new THREE.HemisphereLight(0xddeeff, 0x0f0e0d, 0.02);
        this.scene.add(hemisphereLight);
        return hemisphereLight
    }

    private initControls() {
        const controls = new OrbitControls(this.camera, this.renderer.domElement);
        controls.minDistance = 1;
        controls.maxDistance = 30;
    }

    private createRenderer(): THREE.WebGLRenderer {
        const renderer = new THREE.WebGLRenderer({ antialias: true });
        renderer.physicallyCorrectLights = true;
        renderer.outputEncoding = THREE.sRGBEncoding;
        renderer.shadowMap.enabled = true;
        renderer.toneMapping = THREE.ReinhardToneMapping;
        renderer.setPixelRatio( window.devicePixelRatio );
        renderer.setSize( window.innerWidth, window.innerHeight );
        document.body.appendChild(renderer.domElement);

        return renderer
    }

    private createCamera(): THREE.PerspectiveCamera {
        const camera = new THREE.PerspectiveCamera( 50, window.innerWidth / window.innerHeight, 0.1, 100 );
        camera.position.x = -9;
        camera.position.z = 5;
        camera.position.y = 5;
        return camera
    }

    private onWindowResize() {
        this.camera.aspect = window.innerWidth / window.innerHeight;
        this.camera.updateProjectionMatrix();
        this.renderer.setSize(window.innerWidth, window.innerHeight);
    }
}

export default Scene