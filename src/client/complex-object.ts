import {Group, Mesh, Scene} from "three";
import {degreesToRad} from "./utils";

abstract class ComplexObject {
    private group: Group = new Group()
    private mirror: Group = new Group()

    constructor(
        protected readonly x: number,
        protected readonly y: number,
        protected readonly z: number
    ) {

    }

    public addToScene(scene: Scene) {
        this.group.receiveShadow = true
        this.group.castShadow = true
        scene.add(this.group)
        scene.add(this.mirror)
    }

    public setRotation(degrees: number) {
        this.group.rotation.set(0, degreesToRad(degrees), 0)
        this.mirror.rotation.set(degreesToRad(180), -degreesToRad(degrees), 0)
    }

    public setPosition(x: number, y: number, z: number) {
        this.group.position.set(x, y, z)
        this.mirror.position.set(x, -y, z)
    }

    protected addObject(mesh: Mesh) {
        const copy = mesh.clone()
        this.group.add(mesh)
        this.mirror.add(copy)
    }
}

export default ComplexObject