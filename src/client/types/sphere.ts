export type Sphere = {
    x: number
    y: number
    z: number
    radius: number
    color?: string
    texture?: string
}