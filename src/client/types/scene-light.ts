import {MeshStandardMaterial, PointLight} from "three";

export type SceneLight = {
    light: PointLight
    material: MeshStandardMaterial
}