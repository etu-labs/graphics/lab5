import {Sphere} from "./types/sphere";

export const spheres: Sphere[] = [
    {
        x: -1,
        y: 0.5,
        z: 1,
        radius: 0.6,
        texture: 'wood1.jpg'
    },
    {
        x: 1,
        y: 0.5,
        z: 1,
        radius: 0.8,
        texture: 'wood4.jpg'
    },
    {
        x: 0,
        y: 0.5,
        z: 2,
        radius: 0.5,
        texture: 'wood3.jpg'
    },
    {
        x: -0.1,
        y: 0.5,
        z: 4.8,
        radius: 0.6,
        color: '#2a1e03'
        // texture: 'wood3.jpg'
    },
    {
        x: -1,
        y: 0.3,
        z: 4,
        radius: 0.3,
        color: '#e5c247'
    },

    {
        x: 0,
        y: 0.5,
        z: 3.7,
        radius: 0.5,
        color: '#235266'
    },
    {
        x: -3.1,
        y: 0.5,
        z: 2,
        radius: 0.5,
        color: '#3d1238'
    }
]