import * as THREE from 'three';

import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls'
import { OBJLoader } from 'three/examples/jsm/loaders/OBJLoader'
import Stats from 'three/examples/jsm/libs/stats.module'
import { GUI } from 'dat.gui'
import {BoxGeometry, MeshStandardMaterial, PointLight} from "three";

let camera: THREE.Camera, scene: THREE.Object3D<THREE.Event>, renderer: THREE.WebGLRenderer, bulbLight: THREE.Object3D<THREE.Event>, bulbMat: THREE.MeshStandardMaterial | undefined, hemiLight: THREE.Object3D<THREE.Event>, stats: { dom: any; update: () => void; };
let ballMat: THREE.MeshStandardMaterial | undefined, cubeMat: THREE.MeshStandardMaterial | undefined, floorMat: THREE.MeshStandardMaterial | undefined;

let previousShadowMap = false;


// ref for lumens: http://www.power-sure.com/lumens.htm
const bulbLuminousPowers = {
    '110000 lm (1000W)': 110000,
    '3500 lm (300W)': 3500,
    '1700 lm (100W)': 1700,
    '800 lm (60W)': 800,
    '400 lm (40W)': 400,
    '180 lm (25W)': 180,
    '20 lm (4W)': 20,
    'Off': 0
};

// ref for solar irradiances: https://en.wikipedia.org/wiki/Lux
const hemiLuminousIrradiances = {
    '0.0001 lx (Moonless Night)': 0.0001,
    '0.002 lx (Night Airglow)': 0.002,
    '0.5 lx (Full Moon)': 0.5,
    '3.4 lx (City Twilight)': 3.4,
    '50 lx (Living Room)': 50,
    '100 lx (Very Overcast)': 100,
    '350 lx (Office Room)': 350,
    '400 lx (Sunrise/Sunset)': 400,
    '1000 lx (Overcast)': 1000,
    '18000 lx (Daylight)': 18000,
    '50000 lx (Direct Sun)': 50000
};

const params = {
    shadows: true,
    exposure: 0.68,
    bulbPower: Object.keys( bulbLuminousPowers )[ 2 ],
    hemiIrradiance: Object.keys( hemiLuminousIrradiances )[ 3 ]
};

type SceneLight = {
    light: PointLight
    material: MeshStandardMaterial
}

const lights: SceneLight[] = []

init();
animate();

function init() {

    camera = new THREE.PerspectiveCamera( 50, window.innerWidth / window.innerHeight, 0.1, 100 );
    camera.position.x = - 4;
    camera.position.z = 4;
    camera.position.y = 2;

    scene = new THREE.Scene();

    addLight(-5, 3, 2)
    addLight(0, 2, 6)

    hemiLight = new THREE.HemisphereLight( 0xddeeff, 0x0f0e0d, 0.02 );
    scene.add( hemiLight );

    floorMat = new THREE.MeshStandardMaterial( {
        roughness: 0.8,
        color: '#816656',
        metalness: 0.2,
        bumpScale: 0.0005
    } );
    const textureLoader = new THREE.TextureLoader();
    textureLoader.load( 'textures/hardwood2_diffuse.jpg', function ( map ) {

        map.wrapS = THREE.RepeatWrapping;
        map.wrapT = THREE.RepeatWrapping;
        map.anisotropy = 4;
        map.repeat.set( 10, 24 );
        map.encoding = THREE.sRGBEncoding;
        // @ts-ignore
        // floorMat.map = map;
        // @ts-ignore
        floorMat.needsUpdate = true;

    } );
    textureLoader.load( 'textures/hardwood2_bump.jpg', function ( map ) {

        map.wrapS = THREE.RepeatWrapping;
        map.wrapT = THREE.RepeatWrapping;
        map.anisotropy = 4;
        map.repeat.set( 10, 24 );
        // @ts-ignore
        // floorMat.bumpMap = map;
        // @ts-ignore
        floorMat.needsUpdate = true;

    } );
    textureLoader.load( 'textures/hardwood2_roughness.jpg', function ( map ) {

        map.wrapS = THREE.RepeatWrapping;
        map.wrapT = THREE.RepeatWrapping;
        map.anisotropy = 4;
        map.repeat.set( 10, 24 );
        // @ts-ignore
        // floorMat.roughnessMap = map;
        // @ts-ignore
        floorMat.needsUpdate = true;

    } );

    cubeMat = new THREE.MeshStandardMaterial( {
        roughness: 0.7,
        color: 0xffffff,
        bumpScale: 0.002,
        metalness: 0.2
    } );
    textureLoader.load( 'img/wood4.jpeg', function ( map ) {

        map.wrapS = THREE.RepeatWrapping;
        map.wrapT = THREE.RepeatWrapping;
        map.anisotropy = 4;
        map.repeat.set( 1, 1 );
        map.encoding = THREE.sRGBEncoding;
        // @ts-ignore
        cubeMat.map = map;
        // @ts-ignore
        cubeMat.needsUpdate = true;

    } );

    const floorGeometry = new THREE.PlaneGeometry( 20, 20 );
    const floorMesh = new THREE.Mesh( floorGeometry, floorMat );
    floorMesh.receiveShadow = true;
    floorMesh.rotation.x = - Math.PI / 2.0;
    scene.add( floorMesh );

    const balls = [
        {
            x: -1,
            y: 0.5,
            z: 1,
            radius: 0.6,
            texture: 'wood1.jpg'
        },
        {
            x: 1,
            y: 0.5,
            z: 1,
            radius: 0.8,
            texture: 'wood4.jpg'
        },
        {
            x: 0,
            y: 0.5,
            z: 2,
            radius: 0.5,
            texture: 'wood3.jpg'
        },
        {
            x: -0.1,
            y: 0.5,
            z: 4.8,
            radius: 0.6,
            color: '#2a1e03'
            // texture: 'wood3.jpg'
        },
        {
            x: -1,
            y: 0.3,
            z: 4,
            radius: 0.3,
            color: '#e5c247'
        },

        {
            x: 0,
            y: 0.5,
            z: 3.7,
            radius: 0.5,
            color: '#235266'
        },
        {
            x: -3.1,
            y: 0.5,
            z: 2,
            radius: 0.5,
            color: '#3d1238'
        }
    ]

    for (let i = 0; i < 6; i++) {
        if (i === 0) {
            const boxGeometry = new THREE.BoxGeometry( 5, 1.7, 0.1 );
            const boxMesh = new THREE.Mesh( boxGeometry, cubeMat );
            boxMesh.position.set( 0, 0.5, 0.05 );
            boxMesh.castShadow = true;
            boxMesh.receiveShadow = true;
            scene.add( boxMesh );
        }

        if (i === 1) {
            const boxGeometry = new THREE.BoxGeometry( 5, 1.7, 0.1 );
            const boxMesh = new THREE.Mesh( boxGeometry, cubeMat );
            boxMesh.position.set( 0, 0.5, 2.95 );
            boxMesh.castShadow = true;
            boxMesh.receiveShadow = true;
            scene.add( boxMesh );
        }

        if (i === 2) {
            const boxGeometry = new THREE.BoxGeometry( 3, 1.7, 0.1 );
            const boxMesh = new THREE.Mesh( boxGeometry, cubeMat );
            boxMesh.rotation.set(0, Math.PI / 2, 0)
            boxMesh.position.set( 2.5, 0.5, 1.5 );
            boxMesh.castShadow = true;
            boxMesh.receiveShadow = true;
            scene.add( boxMesh );
        }

        if (i === 3) {
            const boxGeometry = new THREE.BoxGeometry( 3, 1.7, 0.1 );
            const boxMesh = new THREE.Mesh( boxGeometry, cubeMat );
            boxMesh.rotation.set(0, Math.PI / 2, 0)
            boxMesh.position.set( -2.5, 0.5, 1.5 );
            boxMesh.castShadow = true;
            boxMesh.receiveShadow = true;
            scene.add( boxMesh );
        }

        if (i === 4) {
            const boxGeometry = new THREE.BoxGeometry( 5, 1.7, 0.1 );
            const boxMesh = new THREE.Mesh( boxGeometry, cubeMat );
            boxMesh.position.set( 0, 1.9, 0.6 );
            boxMesh.rotation.set(Math.PI / 4, 0, 0)
            boxMesh.castShadow = true;
            boxMesh.receiveShadow = true;
            scene.add( boxMesh );
        }

        if (i === 5) {
            const boxGeometry = new THREE.BoxGeometry( 5, 0.1, 3 );
            const boxMesh = new THREE.Mesh( boxGeometry, cubeMat );
            boxMesh.position.set( 0, 0, 1.5 );
            boxMesh.castShadow = true;
            boxMesh.receiveShadow = true;
            scene.add( boxMesh );
        }
    }

    balls.forEach((ball, i) => {
        const material = new THREE.MeshStandardMaterial( {
            color: ball.color || '#1f2e22',
            roughness: 0.5,
            metalness: 1
        } );

        if (ball.texture) {
            textureLoader.load( 'textures/' + ball.texture, function ( map ) {
                map.anisotropy = 4;
                map.encoding = THREE.sRGBEncoding;
                material.color = new THREE.Color('#fff')
                // @ts-ignore
                material.map = map;
                // @ts-ignore
                material.needsUpdate = true;
            } );
        }

        const ballGeometry = new THREE.SphereGeometry( ball.radius, 32, 32 );
        const ballMesh = new THREE.Mesh( ballGeometry, material );
        ballMesh.position.set( ball.x, ball.radius, ball.z );
        ballMesh.rotation.y = Math.PI;
        ballMesh.castShadow = true;
        ballMesh.receiveShadow = true
        scene.add( ballMesh );
    })

    // const geometry = new THREE.CylinderGeometry( 1, 1, 5, 32 ); // Note: Any detail level above zero results in a "sphere" style of refraction, rather than faceted
    // const material = new THREE.MeshPhysicalMaterial({
    //     roughness: 0,
    //     transmission: 1,
    // });
    // material.thickness = 0.3
    //
    // const mesh = new THREE.Mesh(geometry, material);
    // mesh.position.set(-3, 1, 1.5)
    // scene.add(mesh);

    renderer = new THREE.WebGLRenderer();
    renderer.physicallyCorrectLights = true;
    renderer.outputEncoding = THREE.sRGBEncoding;
    renderer.shadowMap.enabled = true;
    renderer.toneMapping = THREE.ReinhardToneMapping;
    renderer.setPixelRatio( window.devicePixelRatio );
    renderer.setSize( window.innerWidth, window.innerHeight );
    // @ts-ignore
    document.body.appendChild( renderer.domElement );


    const controls = new OrbitControls( camera, renderer.domElement );
    controls.minDistance = 1;
    controls.maxDistance = 20;

    window.addEventListener( 'resize', onWindowResize );


    // @ts-ignore
    // const gui = new GUI();
    //
    // gui.add( params, 'hemiIrradiance', Object.keys( hemiLuminousIrradiances ) );
    // gui.add( params, 'bulbPower', Object.keys( bulbLuminousPowers ) );
    // gui.add( params, 'exposure', 0, 1 );
    // gui.add( params, 'shadows' );
    // gui.open();

}

function onWindowResize() {

    // @ts-ignore
    camera.aspect = window.innerWidth / window.innerHeight;
    // @ts-ignore
    camera.updateProjectionMatrix();

    renderer.setSize( window.innerWidth, window.innerHeight );

}

//

function animate() {

    requestAnimationFrame( animate );

    render();

}

function render() {

    renderer.toneMappingExposure = Math.pow( params.exposure, 5.0 ); // to allow for very bright scenes.
    renderer.shadowMap.enabled = params.shadows;

    if ( params.shadows !== previousShadowMap ) {

        // @ts-ignore
        // ballMat.needsUpdate = true;
        // @ts-ignore
        cubeMat.needsUpdate = true;
        // @ts-ignore
        floorMat.needsUpdate = true;
        previousShadowMap = params.shadows;

    }

    lights.forEach((light) => {
        light.light.castShadow = params.shadows;
        // @ts-ignore
        light.light.power = bulbLuminousPowers[ params.bulbPower ];
        // @ts-ignore
        light.material.emissiveIntensity = light.light.intensity / Math.pow( 0.02, 2.0 ); // convert from intensity to irradiance at bulb surface
    })

    // @ts-ignore
    hemiLight.intensity = hemiLuminousIrradiances[ params.hemiIrradiance ];
    renderer.render( scene, camera );
    // stats.update();
}

function addLight(x: number, y: number, z: number) {
    const bulbGeometry = new THREE.SphereGeometry( 0.1, 32, 32 );
    const light = new THREE.PointLight( 0xffee88, 1, 100, 2 );

    const material = new THREE.MeshStandardMaterial( {
        emissive: 0xffffee,
        emissiveIntensity: 1,
        color: 0x000000
    } );

    light.add( new THREE.Mesh( bulbGeometry, material ) );
    light.position.set( x, y, z );
    light.castShadow = true;
    lights.push({light, material})
    scene.add( light );
}

// const scene = new THREE.Scene();
//
// // Set up the camera
// const camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);
//
// // Set up the renderer
// const renderer = new THREE.WebGLRenderer();
// renderer.setSize(window.innerWidth, window.innerHeight);
// document.body.appendChild(renderer.domElement);
//
// // Set up the light
// const light = new THREE.PointLight(0xffffff, 1, 100);
// light.position.set(10, 10, 10);
// scene.add(light);
//
// // Create the sphere geometry
// const sphereGeometry = new THREE.SphereGeometry(5, 32, 32);
//
// // Create the sphere material
// const sphereMaterial = new THREE.MeshBasicMaterial({ color: 0xffff00 });
//
// // Create the sphere mesh
// const sphere = new THREE.Mesh(sphereGeometry, sphereMaterial);
//
// // Add the sphere to the scene
// scene.add(sphere);
//
// // Render the scene
// const render = function () {
//     requestAnimationFrame(render);
//     renderer.render(scene, camera);
// };
//
// render();