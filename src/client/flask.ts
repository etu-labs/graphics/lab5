import ComplexObject from "./complex-object";
import * as THREE from 'three'
import {degreesToRad} from "./utils";

class Flask extends ComplexObject {
    private readonly smallRadius = 0.3
    private readonly bigRadius = 0.5
    private readonly flaskRadius = 1
    private readonly flaskWidth = 7
    private readonly ligWidth = 0.2
    private readonly ringWidth = 0.05

    constructor(
        x: number,
        y: number,
        z: number,
    ) {
        super(x, y, z)

        this.addGlass()
        this.addBigSphere(-3)
        this.addSmallSphere(-1.5)
        this.addBigSphere(0)
        this.addSmallSphere(1.5)
        this.addBigSphere(3)
        this.addLig((this.flaskWidth + this.ligWidth) / 2)
        this.addRing((this.flaskWidth + this.ringWidth) / 2 + this.ligWidth)
        this.addLig(-(this.flaskWidth + this.ligWidth) / 2)
        this.addLig((this.flaskWidth + this.ligWidth) / 2 + this.ligWidth + this.ringWidth)
        this.addRing(-(this.flaskWidth + this.ringWidth) / 2 - this.ligWidth)
        this.addLig(-(this.flaskWidth + this.ligWidth) / 2 - this.ligWidth - this.ringWidth)
    }

    private addSmallSphere(offset: number) {
        const material = new THREE.MeshStandardMaterial( {
            color: '#ffffff',
            roughness: 0.3,
            metalness: 1
        });

        const geometry = new THREE.SphereGeometry(this.smallRadius, 32, 32 );
        const mesh = new THREE.Mesh(geometry, material);
        mesh.position.set(0, this.flaskRadius, offset);
        mesh.castShadow = true;
        mesh.receiveShadow = true
        this.addObject(mesh)
    }

    private addBigSphere(offset: number) {
        const material = new THREE.MeshStandardMaterial( {
            color: '#008eff',
            roughness: 0.3,
            metalness: 1,
        } );

        const geometry = new THREE.IcosahedronGeometry(this.bigRadius, 32 );
        const mesh = new THREE.Mesh(geometry, material);
        mesh.position.set(0, this.flaskRadius, offset);
        mesh.castShadow = true;
        mesh.receiveShadow = true
        this.addObject(mesh)
    }

    private addGlass() {
        const geometry = new THREE.CylinderGeometry( this.flaskRadius, this.flaskRadius, this.flaskWidth, 32 );
        const material = new THREE.MeshPhysicalMaterial({
            roughness: 0,
            transmission: 0.99,
        });
        material.thickness = 0

        const mesh = new THREE.Mesh(geometry, material);
        mesh.castShadow = true;
        mesh.receiveShadow = true
        mesh.rotation.set(degreesToRad(90), 0, 0)
        mesh.position.set(this.x, this.y + this.flaskRadius, this.z)
        this.addObject(mesh)
    }

    private addLig(offset: number) {
        const geometry = new THREE.CylinderGeometry( this.flaskRadius, this.flaskRadius, this.ligWidth, 32 );
        const material = new THREE.MeshStandardMaterial({
            color: '#000',
            roughness: 0.3,
            metalness: 0.9
        });

        const mesh = new THREE.Mesh(geometry, material);
        mesh.rotation.set(degreesToRad(90), 0, 0)
        mesh.position.set(0, this.flaskRadius, offset)
        mesh.castShadow = true;
        mesh.receiveShadow = true
        this.addObject(mesh)
    }

    private addRing(offset: number) {
        const geometry = new THREE.CylinderGeometry( this.flaskRadius, this.flaskRadius, this.ringWidth, 32 );
        const material = new THREE.MeshStandardMaterial({
            color: '#ffffff',
            roughness: 0.3,
            metalness: 0.9
        });

        const mesh = new THREE.Mesh(geometry, material);
        mesh.rotation.set(degreesToRad(90), 0, 0)
        mesh.position.set(0, this.flaskRadius, offset)
        this.addObject(mesh)
    }
}

export default Flask